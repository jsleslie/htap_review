# HTAP_review

At present, this project examines the HTAP UnitCost database (`HTAPUnitCosts.json`) which is provided in json format.

The `HTAP UnitCosts json exploration` walkthrough the EDA perfomed.

Summary files, located in the `outputs` folder, were generated describing the sources used and the unit cost data in a structured table format.